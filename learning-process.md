## 1. How to Learn Faster with the Feynman Technique

**Question 1: What is the Feynman Technique? Explain in 1 line.**\
**Answer** : The Feynman Technique is a learning method that involves explaining a concept to someone in the simplest language with a mindset of explaining to a 5-year-old child .

## 2. Learning How to Learn TED talk by Barbara Oakley.

**Question 2 : In this video, what was the most interesting story or idea for you?**\
**Answer** : The most interesting idea for me is how Edison used diffuse mode to solve his problem, when he was stuck somewhere is simply took a ball bearing and tried to think about the problem that he was trying to solve while relaxing he would fall asleep and the ball bearing fall from his hand and he woke up and he took that idea and work on it.

**Question 3 : What are active and diffused modes of thinking?**\
**Answer** : In active mode, when we are concentrated on a specific problem and try to reach/ solve the problem step by step.
while in diffuse mode we are not actively concentrating, thinking by taking breaks and trying to make connections to solve problem.

## 3. Learn Anything in 20 hours
**Question 4 : According to the video, what are the steps to take when approaching a new topic? Only mention the points.**

**Answer :** Basic four steps necessary for learning a new concept

1. Breaking down the Topic
2. learn enough to be able to self-practice and self-correct.
3. Removing barriers to practice.
4. Practicing for at least 20 hours.


## 4.Learning Principles in the Bootcamp

**Question 5 : What are some of the actions you can take going forward to improve your learning process?**\
**Answer :** I will use the Feynman technique to learn a new concept and try to explain my friends and I will also try to start switching from active mode (concentrated mode) to diffusion mode.