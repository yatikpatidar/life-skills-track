## Tiny Habits
#### Question 1: In this video, what was the most interesting story or idea for you?
**Answer:** An interesting idea in the above video is establishing a new behavior as a habit, with the existing behavior serving as a prompt for the new action.
Example : 
After I walk out for the day,
I will say "Today will be a great day".

#### Question 2: How can you use B = MAP to make making new habits easier? What are M, A and P.
**Answer:** To make forming new habits easier using B = MAP.
* Motivation (M) is your desire or energy to perform the behavior.
* Ability (A) is how easy or difficult the behavior is to do.
* Prompt (P) is the trigger that reminds you to perform the behavior.
By adjusting these elements, you can make new habits easier to adopt. F

#### Question 3: Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
**Answer:**   When we celebrate, we feel a sense of accomplishment and pride, which boosts our confidence, and motivation will steadily grow to repeat the habit. This positive support makes it more likely that the habit will stick. Essentially, whatever we celebrate becomes more ingrained in our routine, making it easier to maintain and grow over time.

#### Question 4: In this video, what was the most interesting story or idea for you?
**Answer:**  Some most interesting stories or ideas are:

* Most people think they lack motivation when what they really lack is clarity.
 * Habits are the compound interest of self-improvement. Improving 1% every day leads to a 37.78% improvement in yourself over a whole year.
* Good habits make time your ally, Bad habits make time your enemy.  
* You want to put more steps between you and the bad behavior and fewer steps between you and the good behavior.
* Optimize for the starting line, not the finish line. 
* The best way to change long-term behavior is with short-term feedback.

#### Question 5: What is the book's perspective about Identity?
**Answer:**  The book Atomic Habits suggests that we should focus on changing how we see ourselves. Instead of just setting a goal like "I want to run a marathon," we should think "I am a runner." When we believe in this new identity, our habits will follow, making it easier to stick to positive changes.

#### Question 6: Write about the book's perspective on how to make a habit easier to do?
**Answer:**  Instead of making large shifts all at once, we should try to make small steps. We can establish habits without drastic changes to our daily routine by simply adding small extra steps. Over time, these small changes convert into habits.

#### Question 7: Write about the book's perspective on how to make a habit harder to do?
**Answer:**  To make a habit harder to do, we should increase the effort needed to perform it. This could mean adding obstacles, like deleting distracting apps from our phones and using a nested folder structure to access that app. The idea is to make the habit less convenient, so we're less likely to do it.

#### Question 8: Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
**Answer:**  I would like to do Exercise in my daily routine.

some steps which would help me to make it habbit.
1. After brushing my teeth I will do a normal workout for 10 -15 minutes then I will continue my other work.
2. Adding exercise and workouts to my daily routine timetable every 2-3 days.
3. I will reward myself for doing workouts.
4. I will put some equipment for exercise like dumbles, skipping rope, etc. 

#### Question 9: Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
**Answer:** I would like to eliminate mobile screen time in my daily routine. 

some steps which would help me to make it habbit.
1. I will remove all social media applications from the Home Screen and also try to delete some applications.
2. For some necessary applications, I will hide that application, which adds some extra steps and makes it difficult to access.
3. If I am using any social media application I will use it with Focus Mode, which automatically closes the application after 5-10 minutes timer.
4. I will use a Grey-Scale theme( Black&White) on my phone.