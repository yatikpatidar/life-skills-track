# Prevention of Sexual Harrassment

**Question 1: What kinds of behaviour cause sexual harassment?**

**Answer:** 
   * Comments on person's body, sex or gender.
   * Requesting sexual favors.
   * Spreading rumors about a person's personal or sexual life.      
   * Sexual posters or drawings
   * Emails or texts of a sexual nature
   * Screen savers, cartoons.
   * Sexual assault 
   * Impeding or blocking movement
   * Inappropriate touching
   * Sexual gesturing
---

**Question 2: What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

**Answer:** 
* Record the evidence and write down the incident in detail. After that Report it to HR, trusted colleague, or assigned authorities.
  
* Support the victim in all possible ways and encourage them to report the behavior to higher authorities.

