# Grit And Growth Mindset

**Question 1:** Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.\
**Answer:** Grit is a growth mindset, It is the belief that the ability to learn is not fixed and that it can change with your effort. It is not only about success but we also have to be willing to fail, to be wrong, and to start over again with lessons learned.


**Question 2:** Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.\
**Answer:** A growth mindset is the belief in your capacity to learn and grow.
* Fixed Mindset: They believe that skills and intelligence are set and you either have them or you don't, that some people are just naturally good at things, while others are not.
* Growth Mindset: they believe that skills and intelligence are grown and developed.\
So people who are good at something are good because they built that ability, and people who aren't are not good because they haven't done the work.

**Question 3:** What is the Internal Locus of Control? What is the key point in the video?\
**Answer:** The locus of control is essentially the degree to which you believe you have control over your life.
Internal locus of control: Believing in ourselves, our efforts and hard work, and feeling motivated all of the time
External locus of control: Believing outside factors which are not on over control and not giving enough effort and hard work

**Question 4:** What are the key points mentioned by speaker to build growth mindset (explanation not needed).\
**Answer:** Some key points for growth mindset:
* Believe in your ability to figure things out.
* Question your assumptions(today is not a good predictor for tomorrow or the next year or 10 years).
* Develop your own life curriculum .


**Question 5:** What are your ideas to take action and build Growth Mindset?

**Answer:** the four key ingredients to growth: effort, challenges, mistakes, and feedback.
* Effort: The consistent efforts, energy, and focus towards a goal.
* Challenges: Push me beyond my comfort zone.
* Mistakes: Failures provide me opportunities for learning new thing and improvement.
* Feedback: Guide me to refine my progress.