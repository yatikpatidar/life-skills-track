# Listening and Active Communication

## 1. Active Listening

#### Question 1

What are the steps/strategies to do Active Listening? (Minimum 6 points)\
**Answer :**
some points to do active listening 
1. Avoid destruction, focus on the speaker and topic
2. Try not to disturb, let them finish
3. show interest ( door openers)
4. show that you are listening with body language
5. Take notes during an important conversation
6. use a phrase like("tell me more", "and then what happens" etc)

## 2. Reflective Listening

### Application to Business/Software Context


#### Question 2
According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)\
**Answer :**
According to Fisher's model, the key points of Reflective Listening
Try to focus, focus on hearing other people, and what they are saying rather than speaking yourself.
focus on personal things and feelings, try to understand what the speaker wants to say from his experience, and avoid impersonal things.
Try to clarify it by repeating your word once the speaker completed this to ensure you understood it correctly

## 3. Reflection

#### Question 3
What are the obstacles in your listening process?\
**Answer :** 
I am not able to concentrate on a topic for a long time, and after some time I will get self-distracted and try to think about other topics related to that instead of thinking about that topic.

#### Question 4
What can you do to improve your listening?\
**Answer :**
I will improve my listening by medicating and also trying to listen to audio documents, after that, I will try to write down on pen paper.

## 4. Types of Communication

#### Question 5
When do you switch to Passive communication style in your day to day life?\
**Answer :**
when I am with my closest and loved one, I am not able to express my feelings, and not able to say No directly. 

#### Question 6
When do you switch into Aggressive communication styles in your day to day life?\
**Answer :** When I have a load with a specific deadline, I am not working effectively due to disturbance, or when someone is disturbing me again and again for work without giving their efforts, or when i feel frustrated.

#### Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?\
**Answer :** when I am in a mindset like leaving it, I don't want to mess up the situation.

#### Question 8
How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first befor\e answering this question.)\
**Answer :** I will practice the following strategies to be more assertive:
1. Be aware of body language and tone.
2. Speaks up about problematic situations.
3. Establishing boundaries & limitations.
4. Understand my needs properly.